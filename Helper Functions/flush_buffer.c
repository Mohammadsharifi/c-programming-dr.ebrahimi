// C Code to explain why adding  
// "while ( (getchar()) != '\n');" 
// after "scanf()" statement  
// flushes the input buffer 
#include<stdio.h> 
  
int main() 
{ 
    char str[80], ch; 
      
    for (int i = 0; i < 5; i++) {
        // first approach
        scanf(" %c", &str[i]);
        // --------------------
        // // second approach
        // scanf("%c", &str[i]);
        // while ((getchar()) != '\n');
    }   
       
    printf("%s\n", str);
  
    return 0; 
}