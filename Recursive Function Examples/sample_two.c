#include <stdio.h>

int fib(int n) {
    if (n == 0 || n == 1) return 1;
    else {
        return n*fib(n-1);
    }
}


int main()
{
    int n, ans;
    scanf("%d", &n);
    
    ans = fib(n);
    
    printf("%d", ans);
    return 0;
}