#include <stdio.h>

// a program to calculate f(n) = 1! + 2! + 3! + ... + n!;

int fib(int n) {
    if (n == 0 || n == 1) return 1;
    else return n*fib(n-1);
}

int f(int n) {
    if (n == 0) return 0;
    else if (n == 1) return 1;
    else if (n > 0) {
        return f(n-1) + fib(n);
    }
    
    
}

int main()
{
    int n, ans;
    scanf("%d", &n);
    
    for (int i = 1; i <= n; i++) {
        printf("%d : %d\n", i, fib(i));
    }
    
    printf("******************\n");
    
    ans = f(n);
    
    printf("%d : %d", n, ans);
    
    return 0;
}