#include <stdio.h>

int f(int n) {
    if (n != 0) {
        return n + f(n-1);
    } else {
        return n;
    }
}


int main()
{
    int n, ans;
    scanf("%d", &n);
    
    ans = f(n);
    
    printf("%d", ans);
    return 0;
}