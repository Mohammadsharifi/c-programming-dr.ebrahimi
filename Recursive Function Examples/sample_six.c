#include <stdio.h>

// a program to calculate sine and cosine of an angle using recursive functions

double power(double num, int recurse) {
    if (recurse == 0) return 1;
    else if (recurse == 1) return num;
    else if (recurse > 0) {
        return num*power(num, recurse-1);
    }
}

double fib(int num) {
    if (num == 0 || num == 1) return 1;
    else if (num > 0) return num*fib(num-1);
}


double sine(double angle, int sequences) {
    if (sequences == 0) return angle;
    else {
        if (sequences%2 == 0) {
            return sine(angle, sequences-1) + power(angle, 2*sequences+1)/fib(2*sequences+1);
        } else {
            return sine(angle, sequences-1) - power(angle, 2*sequences+1)/fib(2*sequences+1);
        }
    }
}

double cosine(double angle, int sequences) {
    if (sequences == 0) return 1;
    else {
        if (sequences%2 == 0) {
            return cosine(angle, sequences-1) + power(angle, 2*sequences)/fib(2*sequences);
        } else {
            return cosine(angle, sequences-1) - power(angle, 2*sequences)/fib(2*sequences);
        }
    }
}


int main()
{
    double degree, radian, ans;
    scanf("%lf", &degree);
    
    radian = degree*3.14/180.0;
    
    //
    printf("sin(%.2lf) = %.2lf\n", degree, sine(radian, 10));
    
    printf("cos(%.2lf) = %.2lf\n", degree, cosine(radian, 10));


    return 0;
}