#include <stdio.h>

// تابع بازگشتی برای چاپ کردن تابع f(n) = -1 + 2 - 3 + 4 + ... + (-1)^n * n

int f(int n) {
    if (n != 0) {
        if (n%2 == 1) {
            return -n + f(n-1); 
        } else {
            return n + f(n-1);
        }
    }
}

int main()
{
    int n, ans;
    scanf("%d", &n);
    
    ans = f(n);
    
    printf("%d", ans);
    return 0;
}
