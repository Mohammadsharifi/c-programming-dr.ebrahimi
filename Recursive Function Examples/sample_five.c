#include <stdio.h>

// a program to calculate power of a number using recursive functions

int power(int num, int recurse) {
    if (recurse == 0) return 1;
    else if (recurse == 1) return num;
    else if (recurse >= 2) {
        return num*power(num, recurse-1);
    }
}

int main()
{
    int n, k, ans;
    scanf("%d %d", &n, &k);
    
    ans = power(n, k);
    
    printf("%d^%d : %d", n, k, ans);
    
    return 0;
}